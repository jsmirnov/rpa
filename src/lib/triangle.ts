export enum TriangleType {
    EQUILATERAL = "EQUILATERAL",
    ISOSCELE = "ISOSCELE",
    SCALENE = "SCALENE",
    INVALID = "INVALID"
}

export interface Response {
    error?: Error;
    type: TriangleType
}

function isValidNumber(value: any): boolean {
    return Number.isFinite(value) || (Number.isFinite(Number(value)) && typeof value === "string");
}

function isValidTriangleSide(number: number): boolean {
    return number > 0;
}

function parseInput(sides: any[]): number[] {
    const castedSides = [];
    sides.forEach((value: any, idx: number) => {
        if (isValidNumber(value) && isValidTriangleSide(Number(value))) {
            castedSides[idx] = Number(value);
        } else {
            throw new Error(`Value "${value}" is not a number or not a valid triangle side.`)
        }
    });
    return castedSides;
}

function isValidTriangle(a: number, b: number, c: number): boolean {
    return b + c > a && b + a > c && a + c > b;
}

export function getTriangleType(sidesInput: any[]): Response {
    const result: Response = {
        type: TriangleType.INVALID
    };

    if (!sidesInput || sidesInput.length != 3) {
        result.error = new Error('Triangle should have 3 sides');
        return result;
    }


    try {
        const sides: number[] = parseInput(sidesInput);

        const a = sides[0];
        const b = sides[1];
        const c = sides[2];

        if (!isValidTriangle(a, b, c)) {
            throw new Error(`This is not a valid triangle: ${sides.join(', ')}`);
        }

        if (a == b && b == c) {
            result.type = TriangleType.EQUILATERAL;
        } else if (
            a == b ||
            b == c ||
            a == c
        ) {
            result.type = TriangleType.ISOSCELE;
        } else {
            result.type = TriangleType.SCALENE;
        }
    } catch (e) {
        result.error = e;
    }

    return result;
}