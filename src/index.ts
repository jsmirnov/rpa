import {getTriangleType, Response} from "./lib/triangle";

const args: any[] = process.argv.slice(2);
const result: Response = getTriangleType(args);

if (result.error) {
    console.error(result.error.message);
} else {
    console.log(`Triangle type is ${result.type}`);
}


