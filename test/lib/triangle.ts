import {getTriangleType, Response, TriangleType} from "../../src/lib/triangle";
const assert = require("assert");

describe("getTriangleType Tests", () => {
    it("Should return INVALID on wrong amount of sides.", () => {
        [
            null,
            undefined,
            [],
            [1, 2],
            [1, 2, 3, 4]
        ].forEach((input) => {
            const result: Response = getTriangleType(input);
            assert(result.type === TriangleType.INVALID);
        });
    });

    it("Should return INVALID if there are not only numbers provided or not appropriate number given.", () => {
        [
            [null, null, null],
            [undefined, undefined, undefined],
            [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY, Number.MAX_SAFE_INTEGER],
            [true, false, Number.MIN_VALUE],
            ["one", "two", "three"],
            [-3, -4, -5]
        ].forEach((input) => {
            const result: Response = getTriangleType(input);
            assert(result.type === TriangleType.INVALID);
        });
    });

    it("Should return INVALID as not valid triangle sides combination provided.", () => {
        [
            [1, 2, 3],
            [0, 4, 5],
            [5, 5, 15]
        ].forEach((input) => {
            const result: Response = getTriangleType(input);
            assert(result.type === TriangleType.INVALID);
        });
    });

    it("Should return EQUILATERAL for provided set", () => {
        [
            [6, 6, 6]
        ].forEach((input) => {
            const result: Response = getTriangleType(input);
            assert(result.type === TriangleType.EQUILATERAL);
        });
    });

    it("Should return ISOSCELE for provided set", () => {
        [
            [5, 6, 6]
        ].forEach((input) => {
            const result: Response = getTriangleType(input);
            assert(result.type === TriangleType.ISOSCELE);
        });
    });

    it("Should return SCALENE for provided set", () => {
        [
            [3, 4, 5]
        ].forEach((input) => {
            const result: Response = getTriangleType(input);
            assert(result.type === TriangleType.SCALENE);
        });
    });
});