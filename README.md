# rpa

### Execution
How to run on host.
1. install `node.js v8.11.3` via nvm or directly from package manager
2. run `yarn install` to install dependencies
3. run `yarn compile` to compile Typescript to Javascript
4.
Run JS files:
```
node src/index.js 1 2 3
This is not a valid triangle: 1, 2, 3
node src/index.js 3 3 3
Triangle type is EQUILATERAL
node src/index.js 3 3 2
Triangle type is ISOSCELE
node src/index.js 3 4 5
Triangle type is SCALENE
```

Run TS files directly:
```
./node_modules/.bin/ts-node src/index.ts 1 2 3
This is not a valid triangle: 1, 2, 3
./node_modules/.bin/ts-node src/index.ts 3 3 3
Triangle type is EQUILATERAL
./node_modules/.bin/ts-node src/index.ts 3 3 2
Triangle type is ISOSCELE
./node_modules/.bin/ts-node src/index.ts 3 4 5
Triangle type is SCALENE
```

Alternatively, there is an option to not provision environment, but rather execute all commands within docker.

1. is not needed as you would run all commands inside docker
2. `docker run --rm -it -v $(pwd):/app -w /app node:8.11.3-alpine yarn install`
3. `docker run --rm -it -v $(pwd):/app -w /app node:8.11.3-alpine yarn compile`
4.
To run command itself just run it with this prefix:
`docker run --rm -it -v $(pwd):/app -w /app node:8.11.3-alpine`
So for example:
```
docker run --rm -it -v $(pwd):/app -w /app node:8.11.3-alpine node src/index.js 1 2 3
This is not a valid triangle: 1, 2, 3
```

```
docker run --rm -it -v $(pwd):/app -w /app node:8.11.3-alpine ./node_modules/.bin/ts-node src/index.ts 1 2 3
This is not a valid triangle: 1, 2, 3
```

### Testing
To run tests just use `yarn mocha` instead of `node`.

Example with host:
```
yarn mocha
```

Example with docker:
```
docker run --rm -it -v $(pwd):/app -w /app node:8.11.3-alpine yarn mocha
```